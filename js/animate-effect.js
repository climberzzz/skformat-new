$(document).ready(function(){
	var h = $(window).height();


	$(window).scroll(function(){
		if ( ($(this).scrollTop()+h) >= $("#ex2").offset().top) {
			$("#ex2 .post").css({visibility:"visible"});
			$("#ex2 .post").eq(0).addClass('animated bounceInLeft');
			$("#ex2 .post").eq(1).addClass('animated bounceInRight');
		}

		if ( $(this).scrollTop() == 0 ) {
			$("#ex2 .post").each(function(){
				if ( $(this).hasClass('last') ) {
					$(this).removeClass().addClass('post last');
				} else {
					$(this).removeClass().addClass('post');
				}
				$(this).css({visibility:"hidden"});
			});
		}
	});
});
